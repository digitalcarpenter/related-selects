(function ($, window, document, undefined) {
    "use strict";

    $.widget('dc.relatedSelect', {

        options: {},

        widgetEventPrefix: 'relatedselect:',

        _create: function () {
            this.element.addClass('related-select');

            if (this.options.dependants && !$.isArray(this.options.dependants)) {
                this._setOption('dependants', [this.options.dependants]);
            }

            if (this.options.url) {
                this._setOption('url', this.options.url);
            }

            if (!this.options.urlParser) {
                this._setOption('urlParser', this._urlParser);
            }

            this.element.on('change', this._setValueOption);
            this.element.on('relatedselect:dataloaded', this._setSelectedValue);
            this.element.on('relatedselect:complete', this._updateDependants);
        },

        _destroy: function () {
            this.element.remove();
            this._super();
        },

        _setOption: function (key, value) {
            var prev = this.options[key];
            var self = this;
            var fns = {
                url: function () {
                    self._emptyChildren();
                    if (value) {
                        self._loadData(value);  // data is loaded when the url exists and data is loaded
                    } else {
                        self._trigger('dataloaded', null, { // otherwise trigger dataloaded even if empty
                            data: null,
                            children: self.element.children(),
                            instance: self
                        });
                    }
                }
            };

            this._super(key, value);

            if (key in fns) {
                fns[key]();
                this._triggerOptionChanged(key, prev, value);
            }
        },

        _triggerOptionChanged: function (optionKey, previousValue, currentValue) {
            this._trigger('setOption', {type: 'setOption'}, {
                option: optionKey,
                previous: previousValue,
                current: currentValue
            });
        },

        _getData: function (url) {
            return $.getJSON(url);
        },

        _emptyChildren: function () {
            var self = this;
            self.element.empty();
            if (self.options.emptyOption) {
                self.element.append(self.options.emptyOption);
            }
        },

        _appendChildren: function (data) {
            var self = this;
            $.each(data, function (value, text) {
                self.element.append($('<option>').val(value).text(text));
            });
            return self.element;
        },

        _loadData: function (url) {
            var self = this;
            $.when(self._getData(url)).then(
                function (data) {
                    self._appendChildren(data);
                    self._trigger('dataloaded', null, {
                        data: data,
                        children: self.element.children(),
                        instance: self
                    });
                }
            );
        },

        _setSelectedValue: function (event, data) {
            $(event.target).val(data.instance.options.value ? data.instance.options.value : '');
            data.instance._trigger('complete', event, {
                element: $(event.target),
                instance: data.instance
            });
        },

        _setValueOption: function (event) {
            var target = $(event.target);
            var instance = target.data('dc-relatedSelect');
            instance._setOption('value', target.val() ? target.val() : '');
            instance._trigger('complete', event, {
                element: $(event.target),
                instance: instance
            });
        },

        _urlParser: function (baseurl, selection) {
            return selection && selection !== '' ? baseurl + '/' + selection : baseurl;
        },

        _updateDependants: function (event) {
            var target = $(event.target);
            var selection = target.val();
            $.each(target.data('dc-relatedSelect').options.dependants, function (idx, dependant) {
                var instance = dependant.data('dc-relatedSelect');
                if (instance) {
                    instance.update(selection);    // tell the dependants to update themselves based on whatever urlParser they have
                }
            });
        },

        update: function (parentSelection) {
            this._setOptions({
                url: this.options.urlParser(parentSelection),
                value: this.options.value ? this.options.value : ''
            });
        }

    });


})(jQuery, window, document);