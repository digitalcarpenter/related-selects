$(function () {
    "use strict";

    var urlRegex = /^(.*)(\d+)$/;
    var urlPrefix = 'unit/json/';
    var emptyOption = $('<option>').val('').text('- Select One -');
    var fixture = $('#qunit-fixture');

    var urlParser = function (selection) {
        var matches = urlRegex.exec(selection);
        return matches !== null ? urlPrefix + matches[1] + '/' + matches[2] + '.json' : '';
    };

    var setupASelect = function (id, emptyValue) {
        var select = $('<select>');
        if (id) {
            select.prop('id', id);
        }
        if (emptyValue) {
            select.append(emptyValue);
        }
        fixture.append(select);
        return select;
    };

    QUnit.module("related-selects");

    // Test 1
    QUnit.test("jQuery widget dependency available", function (assert) {
        assert.ok($.widget, "Widget is available");
    });

    // Test 2
    QUnit.test("RelatedSelect widget defined on jquery object", function (assert) {
        assert.ok($.fn.relatedSelect, "relatedselect plugin is defined");
    });

    // Test 3
    QUnit.test("On initialisation, should return an element", function (assert) {
        var select = setupASelect();
        assert.ok(select.relatedSelect() === select, "select returned");
    });

    // Test 4
    QUnit.test("On initialisation of relatedselect, the select has a 'related-select' class", function (assert) {
        var select = setupASelect();
        assert.ok(select.relatedSelect().hasClass('related-select'), "select has class 'related-select'");
    });

    // Test 5
    QUnit.test("Can initialise with options", function (assert) {
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        select.relatedSelect({url: url});
        assert.equal(select.relatedSelect('option', 'url'), url, "URL in equals URL out");
    });

    // Test 6
    QUnit.test("Can get an instance of the widget from the element using .data('dc-relatedSelect')", function (assert) {
        var select = setupASelect().relatedSelect();
        assert.ok(select.data('dc-relatedSelect'), 'Instance exists');
    });

    // Test 7
    QUnit.test("Can get data from a URL", function (assert) {
        assert.expect(1);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var instance = select.relatedSelect().data('dc-relatedSelect');
        $.when(instance._getData(url)).then(
            function (data) {
                assert.equal(Object.keys(data).length, 3, 'Expected data count matches');
                done();
            }
        );
    });

    // Test 8
    QUnit.test("Can add option elements to select", function (assert) {
        assert.expect(2);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var instance = select.relatedSelect().data('dc-relatedSelect');
        $.when(instance._getData(url)).then(
            function (data) {
                var dataLength = Object.keys(data).length;
                assert.equal(dataLength, 3, 'Loaded data from URL');
                assert.equal(instance._appendChildren(data).children().length, dataLength, 'Length of select children matches data loaded');
                done();
            }
        );
    });

    // Test 9
    QUnit.test("Can load data from a URL directly into select options and inspect 'relatedselect:dataloaded' event", function (assert) {
        assert.expect(1);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var instance = select.relatedSelect().data('dc-relatedSelect');
        select.on('relatedselect:dataloaded', function (event, data) {
            assert.equal($(event.target).children().length, 3, 'Select has 3 option children');
            done();
        });
        instance._loadData(url);
    });

    // Test 10
    QUnit.test("Can load data by changing/setting the 'url' option", function (assert) {
        assert.expect(1);
        var done = assert.async(1);
        var select = setupASelect();
        select.on('relatedselect:dataloaded', function (event, data) {
            assert.equal($(event.target).children().length, 3, 'Select has 3 option children');
            done();
        });
        select.relatedSelect();
        select.relatedSelect('option', 'url', urlPrefix + 'top.json');
    });

    // Test 11
    QUnit.test("Can publish 'relatedselect:setoption' when options change", function (assert) {
        assert.expect(2);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        select.on('relatedselect:setoption', function (event, data) {
            assert.equal($(event.target).get(0), select.get(0), 'Target equals the select element');
            assert.deepEqual(data, {
                option: 'url',
                previous: undefined,
                current: url
            }, 'Event data is equal');
            done();
        });
        select.relatedSelect();
        select.relatedSelect({url: url});
    });

    // Test 12
    QUnit.test("Handles empty value option option", function (assert) {
        assert.expect(2);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        select.on('relatedselect:dataloaded', function (event, data) {
            var target = $(event.target);
            assert.equal(target.children().length, 4, 'Select has 4 option children');
            assert.deepEqual(target.children().get(0), emptyOption.get(0), 'The first option is an empty option');
            done();
        });
        select.relatedSelect({url: url, emptyOption: emptyOption});
    });

    // Test 13
    QUnit.test("Handles setting the selected value from the 'value' option triggered by 'valueset' event", function (assert) {
        assert.expect(2);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var value = 'a2';
        select.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.children().length, 4, 'Select has 4 option children');
            assert.equal(target.val(), value, 'Selected value matches');
            done();
        });
        select.relatedSelect({url: url, emptyOption: emptyOption, value: value});
    });

    // Test 14
    QUnit.test("Bubbles-up the 'change' event", function (assert) {
        assert.expect(1);
        var done = assert.async(1);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var value = 'a2';
        select.on('relatedselect:complete', function (event, data) {
            $(event.target).val(value).change();
        });
        select.on('relatedselect:change', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), value, 'Value matches selected option');
            done();
        });
        select.relatedSelect({url: url, emptyOption: emptyOption});
    });

    // Test 15
    QUnit.test("On change of the selected option, the value option is changed accordingly", function (assert) {
        assert.expect(3);
        var done = assert.async(2);
        var select = setupASelect();
        var url = urlPrefix + 'top.json';
        var origValue = '';
        var newValue = 'a3';
        select.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), origValue, "On complete, original value matches 'value' option");
            // Simulate a
            $(event.target).val(newValue).change();
            done();
        });
        select.on('relatedselect:change', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), newValue, 'On change, selected value matches the new value');
            assert.equal(select.relatedSelect('option', 'value'), newValue, "On change, the 'value' option matches the new value")
            done();
        });
        select.relatedSelect({url: url, emptyOption: emptyOption, value: origValue});
    });

    // Test 16
    QUnit.test("Can configure a single related select", function(assert) {
        assert.expect(2);
        var parentSelect = setupASelect('parent');
        var parentUrl = urlPrefix + 'top.json';
        var childSelect = setupASelect('child');
        assert.equal(fixture.find('select').length, 2, 'There are 2 selects attached to the fixture');
        parentSelect.relatedSelect({url: parentUrl, dependants: childSelect});
        assert.deepEqual(parentSelect.relatedSelect('option', 'dependants'), [childSelect], 'A single child select was added to the array of dependants');
    });

    // Test 17
    QUnit.test("Can configure multiple related selects", function(assert) {
        assert.expect(2);
        var parentSelect = setupASelect('parent');
        var parentUrl = urlPrefix + 'top.json';
        var childSelectA = setupASelect('childA');
        var childSelectB = setupASelect('childB');
        assert.equal(fixture.find('select').length, 3, 'There are 3 selects attached to the fixture');
        parentSelect.relatedSelect({url: parentUrl, dependants: [childSelectA, childSelectB]});
        assert.deepEqual(parentSelect.relatedSelect('option', 'dependants'), [childSelectA, childSelectB], 'Multiple children selects were added to the array of dependants');
    });

    // Test 18
    QUnit.test("On change of the parent select, modify the url option of the childSelect as per childSelect's configured urlParser", function (assert) {
        assert.expect(2);
        var done = assert.async(2);
        var parentSelect = setupASelect('parent');
        var parentUrl = urlPrefix + 'top.json';
        var childSelect = setupASelect('child');
        var origValue = '';
        var newValue = 'a1';

        parentSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), origValue, "On complete, original value matches 'value' option");
            $(event.target).val(newValue).trigger('change');
            done();
        });
        childSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.relatedSelect('option', 'url'), urlPrefix + 'a/1.json', 'On completed setup the childSelect will be configured with a URL');
            done();
        });
        childSelect.relatedSelect({urlParser: urlParser});
        parentSelect.relatedSelect({url: parentUrl, emptyOption: emptyOption, value: origValue, dependants: [childSelect]});
    });

    // Test 19
    QUnit.test("On change of the parent select, load the childSelect's data into its select options", function (assert) {
        assert.expect(2);
        var done = assert.async(2);
        var parentSelect = setupASelect('parent');
        var parentUrl = urlPrefix + 'top.json';
        var childSelect = setupASelect('child');
        var origValue = '';
        var newValue = 'a1';

        parentSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), origValue, "On complete, original value matches 'value' option");
            $(event.target).val(newValue).trigger('change');
            done();
        });
        childSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.children('option').length, 4, 'On completed setup the childSelect will have child options including empty');
            done();
        });
        parentSelect.relatedSelect({url: parentUrl, emptyOption: emptyOption, value: origValue, dependants: [childSelect]});
        childSelect.relatedSelect({urlParser: urlParser, emptyOption: emptyOption});
    });

    // Test 20
    QUnit.test("On change of a topmost parent, all descendants will be updated even with empty values", function (assert) {
        assert.expect(3);
        var done = assert.async(3);
        var parentSelect = setupASelect('parent');
        var parentUrl = urlPrefix + 'top.json';
        var childSelect = setupASelect('child');
        var grandchildSelect = setupASelect('grandchild');
        var origValue = '';
        var newValue = 'a1';

        parentSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.val(), origValue, "On complete, original value matches 'value' option");
            $(event.target).val(newValue).trigger('change');
            done();
        });
        childSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.children('option').length, 4, 'On updated setup the childSelect will have child options including empty');
            done();
        });
        grandchildSelect.on('relatedselect:complete', function (event, data) {
            var target = $(event.target);
            assert.equal(target.children('option').length, 1, 'On child select being updated, the grandchildSelect will only have child empty option as the child will not have selected anything yet.');
            done();
        });

        parentSelect.relatedSelect({url: parentUrl, emptyOption: emptyOption, value: origValue, dependants: [childSelect]});
        childSelect.relatedSelect({urlParser: urlParser, emptyOption: emptyOption, dependants: [grandchildSelect]});
        grandchildSelect.relatedSelect({urlParser: urlParser, emptyOption: emptyOption});
    });

});

